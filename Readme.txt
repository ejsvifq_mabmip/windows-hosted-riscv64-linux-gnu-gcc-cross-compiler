This is a canadian riscv64 cross GCC toolchain (multilib). Built by the fast_io library's author

Unix Timestamp:1653022936.048252645
UTC:2022-05-20T05:02:16.048252645Z

fast_io:
https://github.com/cppfastio/fast_io.git

build	:	x86_64-linux-gnu
host	:	x86_64-w64-mingw32
target	:	riscv64-linux-gnu
